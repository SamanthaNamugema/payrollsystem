﻿Public Class NewMonth
    Public Sql As New SQLControl
    Public decAmount As Decimal
    Public decBalance As Decimal
    Public bill_no As Integer
    Public decTotalSalary As Decimal
    Private Sub NewMonth_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub txtAmount_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtAmount.TextChanged
        Try
            decAmount = CDec(txtAmount.Text)
        Catch ex As Exception
            decAmount = 0
        End Try

        If (decAmount > decBalance) Then
            lblWarning.Text = "The amount you have entered is more than your balance"
        Else
            lblWarning.Text = ""
        End If
    End Sub

    Private Sub btnPayment_Click(sender As System.Object, e As System.EventArgs) Handles btnPayment.Click
        Try
            Dim regDate As Date = Date.Today()

            Sql.AddParams("@bill_no", bill_no)
            Sql.AddParams("@amount", decAmount)
            Sql.AddParams("@date", regDate)

            Sql.ExecuteInsert("InsertPayment")

            btnPayment.Enabled = False
            MessageBox.Show("You have made payment")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class