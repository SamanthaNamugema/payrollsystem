﻿Public Class ViewPaymentEmployees
    Public Sql As New SQLControl

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim newtext As String = "%" + txtFirstName.Text + "%"

            Sql.AddParams("@first_name", newtext)

            Dim dataTable As DataTable = Sql.getDataTable("searchName")

            If (dataTable.Rows.Count > 0) Then
                dgv.DataSource = dataTable
            Else
                MessageBox.Show("The record does not exist")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub dgv_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Try
            Dim emp_id As Integer
            Dim first_name As String
            Dim last_name As String
            Dim salary As Integer
            Dim ttsalary As Decimal
            Dim status As String = ""
            Dim balance As Decimal
            Dim bill_no As Integer
            Dim duration As Integer

            emp_id = dgv.CurrentRow.Cells("emp_id").Value()

            Sql.AddParams("@emp_id", emp_id)

            Dim dataTable As DataTable = Sql.getDataTable("selectNewestPayment")

            first_name = dataTable.Rows(0).Item("first_name")
            last_name = dataTable.Rows(0).Item("last_name")
            salary = dataTable.Rows(0).Item("salary")
            ttsalary = dataTable.Rows(0).Item("ttsalary")
            duration = dataTable.Rows(0).Item("duration")
            balance = dataTable.Rows(0).Item("balance")
            bill_no = dataTable.Rows(0).Item("bill_no")

            If (balance = 0) Then
                'MessageBox.Show("The payments do not exist")
                status = "PAID"
                NewPayment.txtAmount.Enabled = False
                NewPayment.btnPayment.Enabled = False
                NewPayment.btnMonth.Visible = True

            ElseIf (balance = salary) Then
                status = "NOT PAID"
            ElseIf (0 < balance < salary) Then
                status = "PART PAID"
            End If

            NewPayment.txtEmpID.Text = emp_id
            NewPayment.txtFirstName.Text = first_name
            NewPayment.txtLastName.Text = last_name
            NewPayment.txtTotalSalary.Text = ttsalary
            NewPayment.txtSalary.Text = salary
            NewPayment.lblBalance.Text = balance
            NewPayment.lblStatus.Text = status

            NewPayment.getInitVariables(balance, bill_no, ttsalary)

            'get Month/Bill count
            Sql.AddParams("@emp_id", emp_id)

            Dim number As Integer = Sql.getDataTable("getEmpBills").Rows.Count()
            NewPayment.lblMonth.Text = "MONTH " & number & ""
        Catch ex As Exception

        End Try
        Me.Close()
    End Sub

    Private Sub ViewPaymentEmployees_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class