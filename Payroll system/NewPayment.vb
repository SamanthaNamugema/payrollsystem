﻿Public Class NewPayment
    Public Sql As New SQLControl
    Private decAmount As Decimal
    Private decBalance As Decimal
    Private bill_no As Integer
    Private decTotalSalary As Decimal

    Public Sub getInitVariables(balance As Decimal, billno As Integer, totalSalary As Decimal)
        decBalance = balance
        bill_no = billno
        decTotalSalary = totalSalary
    End Sub
    Private Sub TextBox2_TextChanged(sender As System.Object, e As System.EventArgs) Handles txtAmount.TextChanged
        Try
            decAmount = CDec(txtAmount.Text)
        Catch ex As Exception
            decAmount = 0
        End Try

        If (decAmount > decBalance) Then
            lblWarning.Text = "The amount you have entered is more than your balance"
        Else
            lblWarning.Text = ""
        End If
    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        ViewPaymentEmployees.Show()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnPayment.Click
        Try
            Dim regDate As Date = Date.Today()

            Sql.AddParams("@bill_no", bill_no)
            Sql.AddParams("@amount", decAmount)
            Sql.AddParams("@date", regDate)

            Sql.ExecuteInsert("InsertPayment")

            btnPayment.Enabled = False
            MessageBox.Show("You have made payment")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub NewPayment_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        
    End Sub

    Private Sub btnMonth_Click(sender As System.Object, e As System.EventArgs) Handles btnMonth.Click
        Try
            If (decTotalSalary > 0) Then
                Me.btnMonth.Visible = False
                NewMonth.txtEmpID.Text = txtEmpID.Text
                NewMonth.txtFirstName.Text = txtFirstName.Text
                NewMonth.txtLastName.Text = txtLastName.Text
                NewMonth.txtTotalSalary.Text = txtTotalSalary.Text
                NewMonth.decTotalSalary = txtTotalSalary.Text
                NewMonth.txtSalary.Text = txtSalary.Text
                NewMonth.lblBalance.Text = txtSalary.Text
                NewMonth.decBalance = txtSalary.Text
                NewMonth.lblStatus.Text = "NOT PAID"

                'insert new bill
                Sql.AddParams("@emp_id", NewMonth.txtEmpID.Text)
                Sql.AddParams("@salary", NewMonth.txtSalary.Text)
                Sql.AddParams("@balance", NewMonth.decBalance)
                Sql.ExecuteInsert("insertBill")

                NewMonth.bill_no = Me.bill_no + 1

                'get month/bill count
                Sql.AddParams("@emp_id", CInt(txtEmpID.Text))

                Dim number As Integer = Sql.getDataTable("getEmpBills").Rows.Count()
                NewMonth.lblMonth.Text = "MONTH " & number & ""
                NewMonth.Show()

            Else
                MessageBox.Show("Your contract is finished")
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
End Class