﻿Public Class NewEmployee
    Public SQL As New SQLControl
    Private Sub NewEmployee_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Try
            MdiParent = Form1
            cbxSalary.Items.Add(0)
            cbxSalary.Items.Add(300000)
            cbxSalary.Items.Add(500000)
            cbxSalary.Items.Add(700000)
            cbxSalary.Items.Add(1000000)

            cbxDuration.Items.Add(5)
            cbxDuration.Items.Add(10)
            cbxDuration.Items.Add(30)

            Dim dataTable As DataTable = SQL.getDataTable("getlastID")

            If (dataTable.Rows.Count > 0) Then
                txtEmpID.Text = dataTable.Rows(0).Item("emp_id") + 1
            Else
                txtEmpID.Text = 1
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        Try
            Dim ttSalary As Decimal
            ttSalary = cbxSalary.SelectedItem * cbxDuration.SelectedItem

            'Store Employee BioData
            SQL.AddParams("@first_name", txtFirstName.Text)
            SQL.AddParams("@last_name", txtLastName.Text)
            SQL.AddParams("@dob", dtDOB.Value)
            SQL.AddParams("@duration", cbxDuration.SelectedItem)
            SQL.AddParams("@ttsalary", ttSalary)

            SQL.ExecuteInsert("InsertEmployee")
            MessageBox.Show("The record is inserted")


        Catch ex As Exception
            MessageBox.Show("Error inserting an employee: " + ex.Message)
        End Try
    End Sub

    Private Sub cbxDuration_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxDuration.SelectedIndexChanged
        txtTotalSalary.Text = cbxSalary.SelectedItem * cbxDuration.SelectedItem
    End Sub
End Class