﻿Public Class UpdateEmployee
    Public SQL As New SQLControl
    Private Sub UpdateEmployee_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        MdiParent = Form1
        cbxSalary.Items.Add(0)
        cbxSalary.Items.Add(300000)
        cbxSalary.Items.Add(500000)
        cbxSalary.Items.Add(700000)
        cbxSalary.Items.Add(1000000)

        cbxDuration.Items.Add(5)
        cbxDuration.Items.Add(10)
        cbxDuration.Items.Add(30)


    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        ViewEmployees.Show()
    End Sub

    Private Sub cbxDuration_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxDuration.SelectedIndexChanged
        txtTotalSalary.Text = cbxSalary.SelectedItem * cbxDuration.SelectedItem
    End Sub

    Private Sub btnUpdate_Click(sender As System.Object, e As System.EventArgs) Handles btnUpdate.Click
        Try
            'store updated employee biodata
            SQL.AddParams("@emp_id", CInt(txtEmpID.Text))
            SQL.AddParams("@first_name", txtFirstName.Text)
            SQL.AddParams("@last_name", txtLastName.Text)
            SQL.AddParams("@dob", dtDOB.Value)
            SQL.AddParams("@duration", cbxDuration.SelectedItem)
            SQL.AddParams("@ttsalary", CDec(txtTotalSalary.Text))

            SQL.ExecuteUpdate("UpdateEmployee")

            MessageBox.Show("The record has been updated")
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
        
    End Sub

    Private Sub cbxSalary_SelectedIndexChanged(sender As System.Object, e As System.EventArgs) Handles cbxSalary.SelectedIndexChanged
        txtTotalSalary.Text = cbxSalary.SelectedItem * cbxDuration.SelectedItem
    End Sub
End Class