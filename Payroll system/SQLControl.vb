﻿Option Strict On
Imports System.Data.SqlClient

Public Class SQLControl
    Private DBCon As New SqlConnection("Server=LAPTOP-70488OL3;Database=payroll_system;Trusted_Connection=True;")
    Private DBCmd As SqlCommand
    Private DBDA As SqlDataAdapter
    Private DT As DataTable
    Private Params As New List(Of SqlParameter)
    Private NumberBills As Integer

    Public Sub ExecuteInsert(command As String)

        Try
            DBCmd = New SqlCommand
            DBCmd.Connection = DBCon
            DBCmd.CommandText = command
            DBCmd.CommandType = CommandType.StoredProcedure

            Params.ForEach(Sub(p) DBCmd.Parameters.Add(p))
            Params.Clear()

            DBCon.Open()
            DBCmd.ExecuteNonQuery()
            

        Catch ex As Exception
            Throw ex
        Finally
            If DBCon.State = ConnectionState.Open Then
                DBCon.Close()
            End If
        End Try

    End Sub
    Public Sub ExecuteUpdate(command As String)

        Try
            DBCmd = New SqlCommand
            DBCmd.Connection = DBCon
            DBCmd.CommandText = command
            DBCmd.CommandType = CommandType.StoredProcedure

            Params.ForEach(Sub(p) DBCmd.Parameters.Add(p))
            Params.Clear()

            DBCon.Open()
            DBCmd.ExecuteNonQuery()

        Catch ex As Exception
            Throw ex
        Finally
            If DBCon.State = ConnectionState.Open Then
                DBCon.Close()
            End If
        End Try

    End Sub
    Public Function getDataTable(command As String) As DataTable
        Try
            DBCmd = New SqlCommand
            DBCmd.Connection = DBCon
            DBCmd.CommandText = command
            DBCmd.CommandType = CommandType.StoredProcedure

            Params.ForEach(Sub(p) DBCmd.Parameters.Add(p))
            Params.Clear()

            DBCon.Open()
            DBDA = New SqlDataAdapter(DBCmd)
            DT = New DataTable
            DBDA.Fill(DT)
            Return DT
        Catch ex As Exception
            Throw ex
        Finally
            If DBCon.State = ConnectionState.Open Then
                DBCon.Close()
            End If
        End Try
    End Function
    Public Sub AddParams(Name As String, Value As Object)
        Dim NewParam As New SqlParameter(Name, Value)
        Params.Add(NewParam)
    End Sub

End Class
