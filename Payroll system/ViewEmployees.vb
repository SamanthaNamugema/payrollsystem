﻿Public Class ViewEmployees
    Public SQL As New SQLControl
    Private Sub ViewEmployees_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub btnSearch_Click(sender As System.Object, e As System.EventArgs) Handles btnSearch.Click
        Try
            Dim newtext As String = "%" + txtFirstName.Text + "%"

            SQL.AddParams("@first_name", newtext)
            Dim dataTable As DataTable = SQL.getDataTable("searchName")

            If (dataTable.Rows.Count > 0) Then
                dgv.DataSource = dataTable
            Else
                MessageBox.Show("The record does not exist")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub
    'Public Sub dgv_SelectionChanged(object sender, EventArgs e) Handles dgv

    Private Sub dgv_CellClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick
        Dim emp_id As Integer
        Dim first_name As String
        Dim last_name As String
        Dim dob As Date
        Dim salary As Integer
        Dim duration As Integer
        Dim ttsalary As Decimal

        emp_id = dgv.CurrentRow.Cells("emp_id").Value()
        first_name = dgv.CurrentRow.Cells("first_name").Value()
        last_name = dgv.CurrentRow.Cells("last_name").Value()
        dob = dgv.CurrentRow.Cells("dob").Value()
        duration = dgv.CurrentRow.Cells("duration").Value()
        ttsalary = dgv.CurrentRow.Cells("ttsalary").Value()
        salary = ttsalary / duration

        UpdateEmployee.txtEmpID.Text = emp_id
        UpdateEmployee.txtFirstName.Text = first_name
        UpdateEmployee.txtLastName.Text = last_name
        UpdateEmployee.dtDOB.Value = dob
        UpdateEmployee.cbxSalary.SelectedItem = salary
        UpdateEmployee.cbxDuration.SelectedItem = duration
        UpdateEmployee.txtTotalSalary.Text = ttsalary
        Me.Close()

    End Sub
End Class