drop proc InsertEmployee
go

create proc InsertEmployee
(
@first_name varchar(14),
@last_name varchar(16),
@dob date,
@duration int,
@ttsalary decimal
)
as
begin
declare @emp_id int
declare @salary decimal
declare @balance decimal

	Insert into Employee(first_name,last_name,dob,duration,ttsalary)
	values(@first_name,@last_name,@dob,@duration,@ttsalary)

	set @emp_id = (select top 1 emp_id from Employee order by emp_id desc)
	set @salary = @ttsalary/@duration
	set @balance = @salary

	Insert into Bill(emp_id,salary,balance)
	values(@emp_id,@salary,@balance)

	return 0
end
go

drop proc UpdateEmployee
go

create proc UpdateEmployee
(
@emp_id int,
@first_name varchar(14),
@last_name varchar(16),
@dob date,
@duration int,
@ttsalary decimal
)
as
begin
declare @ErrorMsg varchar(200)
	
	if not exists(select emp_id from Employee where emp_id = @emp_id)
	 begin
	 set @ErrorMsg = 'The Employee ID: %d you does not exist in registered Employees'
	  raiserror(@ErrorMsg, 16,1, @emp_id)
		return 1
	 end

	update Employee set first_name = @first_name, last_name = @last_name, dob = @dob, duration = @duration, ttsalary = @ttsalary
	where emp_id = @emp_id
end
go

drop proc getlastID
go

create proc getlastID
as
begin
	select top 1 emp_id from employee order by emp_id DESC
end
go

drop proc searchName
go
create proc searchName
(
	@first_name varchar(14)
)
as
begin
	if @first_name is null
	begin
		select * from Employee
	end
	else
	begin
		select * from Employee where first_name LIKE @first_name
	end
			
end
go


drop proc InsertPayment
go

create proc InsertPayment
(
@bill_no int,
@amount decimal,
@date date
)
as
begin
declare @emp_id int
declare @balance decimal
declare @totalsalary decimal
declare @ErrorMsg varchar(200)

	

	if not exists(select bill_no from Bill where bill_no = @bill_no)
	 begin
	 set @ErrorMsg = 'The Bill No: %d you does not exist in registered Bills'
	  raiserror(@ErrorMsg, 16,1, @bill_no)
		return 1
	 end

	 set @balance = isnull((select balance from Bill where bill_no = @bill_no),0)
	set @totalsalary = (select ttsalary from Employee where emp_id = @emp_id) - @amount

	 	if (@amount > @balance)
	begin
	    set @ErrorMsg = 'The amount you have entered is greater than your balance'
		raiserror(@ErrorMsg, 16,1)
		return 1
	end

	 begin tran

		insert into Payment(bill_no,amount,date)
		values(@bill_no,@amount,@date)
	

	set @emp_id = (select emp_id from Bill where bill_no = @bill_no)
	set @balance = (select salary from Bill where bill_no = @bill_no) - (select sum(amount) from Payment where bill_no = @bill_no)
    set @totalsalary = (select ttsalary from Employee where emp_id = @emp_id) - @amount
	
		update  Bill set balance = @balance where bill_no = @bill_no
		update Employee set ttsalary = @totalsalary where emp_id = @emp_id
	
	if @@Error>0
	begin
	rollback tran
	return 1
	end
	commit tran
	return 0
end
go

drop proc selectNewestPayment
go

create proc selectNewestPayment
(
@emp_id int
)
as
begin
declare @ErrorMsg varchar(200)
	
	if not exists(select emp_id from Employee where emp_id = @emp_id)
	 begin
	 set @ErrorMsg = 'The Employee ID: %d you does not exist in registered Employees'
	  raiserror(@ErrorMsg, 16,1, @emp_id)
		return 1
	 end
	
	if not exists(select emp_id from Bill where emp_id = @emp_id)
	 begin
	 set @ErrorMsg = 'The Employee ID: %d you does not exist in registered Bills'
	  raiserror(@ErrorMsg, 16,1, @emp_id)
		return 1
	 end

	select top 1 * from Employee inner join Bill
     on (Employee.emp_id =	Bill.emp_id)
	 where Employee.emp_id = @emp_id
	 order by bill_no DESC
end
go

drop proc insertBill
go

create proc insertBill
(
@emp_id int,
@salary decimal,
@balance decimal
)
as
begin
declare @ErrorMsg varchar(200)
	
	if not exists(select emp_id from Employee where emp_id = @emp_id)
	 begin
	 set @ErrorMsg = 'The Employee ID: %d you does not exist in registered Employees'
	  raiserror(@ErrorMsg, 16,1, @emp_id)
		return 1
	 end

	insert into Bill(emp_id,salary,balance)
	values(@emp_id,@salary,@balance)
end
go

drop proc getEmpBills
go
create proc getEmpBills
(
@emp_id int
)
as
begin
declare @ErrorMsg varchar(200)
	
	if not exists(select emp_id from Bill where emp_id = @emp_id)
	 begin
	 set @ErrorMsg = 'The Employee ID: %d you does not exist in registered Bills'
	  raiserror(@ErrorMsg, 16,1, @emp_id)
		return 1
	 end

	select * from Bill where emp_id = @emp_id

	return 0
end
go

