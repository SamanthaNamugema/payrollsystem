create database payroll_system
go

use payroll_system
go

drop table Employee
go
create table Employee
(
	emp_id int identity(1,1),
	first_name varchar(14),
	last_name varchar(16),
	dob date,
	duration int,
	ttsalary decimal
	primary key(emp_id)
)

drop table Bill
go

create table Bill
(
bill_no int identity(1,1),
emp_id int,
salary decimal,
balance decimal
primary key(bill_no)
foreign key(emp_id) references Employee(emp_id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
)

drop table Payment
go
create table Payment
(
pay_no int identity(1,1),
bill_no int,
amount decimal,
date date
	primary key(pay_no),
	foreign key(bill_no) references Bill(bill_no)
		ON DELETE CASCADE
		ON UPDATE CASCADE
)



